#!/bin/bash

showname=$1
#set=$2

#set=NeuroSS20
#set=KogsysSS20
#set=KogsysSS19
#set=KogsysSS18

#cp /project/wmt2012/LT/DIVAKIT/$set/$1.mp4 /export/data1/www/lecturetranslator_new/temp/DIVAKIT/$set/
#cp /project/wmt2012/LT/DIVAKIT/$set/$1.wav /export/data1/www/lecturetranslator_new/temp/DIVAKIT/$set/
#cp /project/wmt2012/LT/DIVAKIT/$set/$1.m4u /export/data1/www/lecturetranslator_new/temp/DIVAKIT/$set/

scp tha@i13pc224.ira.uka.de:/project/wmt2012/LT/DIVAKIT/my_transcription/$1.timed.txt /var/www/lecturetranslator/media/eventintegration/transcriptions/


scp tha@i13pc224.ira.uka.de:/project/wmt2012/LT/DIVAKIT/vtt/$1.vtt /var/www/lecturetranslator/media/eventintegration/vtt/
#cp /project/wmt2012/LT/DIVAKIT/vtt/$1.vtt /export/data1/www/lecturetranslator_new/temp/DIVAKIT/vtt/


scp tha@i13pc224.ira.uka.de:/project/wmt2012/LT/DIVAKIT/translations/$1.translated.?? /var/www/lecturetranslator/media/eventintegration/translations/

#cp /project/wmt2012/LT/DIVAKIT/translations/$1.translated.?? /export/data1/www/lecturetranslator_new/temp/DIVAKIT/translations/

#cp /project/wmt2012/LT/DIVAKIT/punct/$1.prepro.punc /export/data1/www/lecturetranslator_new/temp/DIVAKIT/punct/
