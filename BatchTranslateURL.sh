#!/bin/bash
input="lst_Ubi.lst"
while IFS= read -r line
do
  IFS=', ' read -r -a line <<< "$line"
  showname=${line[0]}
  eventID=${line[1]}
  url=${line[2]}
  echo "$showname"
  echo "$eventID"
  wget -O ../media/eventintegration/$showname.mp4 $url
  ./TranslationClickURL.sh $showname $eventID $url 6 de en fr es it </dev/null
done < "$input"

