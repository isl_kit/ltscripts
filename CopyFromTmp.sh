#!/bin/bash

showname=$1
DIVAKIT=$2
#cp /tmp/${showname}.mp4 /export/data1/www/lecturetranslator_new/temp/DIVAKIT/

#<<<<<<< Updated upstream
cp /tmp/${showname}.timed.txt "${DIVAKIT}/transcriptions/"

cp /tmp/${showname}.vtt "${DIVAKIT}/vtt/"

cp /tmp/${showname}.translated.?? "${DIVAKIT}/translations/" || echo "No translations to copy, but continuing anyway"
#=======
#cp /tmp/${showname}.timed.txt "${DIVAKIT}/transcriptions/" 
#
#cp /tmp/${showname}.vtt "${DIVAKIT}/vtt/" 
#
#cp /tmp/${showname}.translated.?? "${DIVAKIT}/translations/" 
#>>>>>>> Stashed changes

