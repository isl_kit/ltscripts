#!/bin/bash

ffmpeg -i $1.mp4 -vn -acodec pcm_s16le -ar 16000 -ac 1 $1.wav
