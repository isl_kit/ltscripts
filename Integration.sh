#!/bin/bash

showname=$1
eventID=$2
src_lang=$3
trg_lang=$4

case $src_lang in
    en)
        fingerprint="en-EN-lecture_KIT"
        ;;
    de) 
        fingerprint="de-DE-lecture_KIT"
        ;;
    fr)
        fingerprint="fr-FR-lecture_KIT"
        ;;
    zh)
        fingerprint="zh-ZH-lecture_KIT"
        ;;
    es)
        fingerprint="es-ES-lecture_KIT"
        ;;
    it)
        fingerprint="it-IT-lecture_KIT"
        ;;
esac

DIVAKIT=/export/data1/www/lecturetranslator_new/temp/DIVAKIT/

bash $DIVAKIT/CopyFromTha.sh $showname 
bash $DIVAKIT/TransferTime.sh $showname $4

ROOT=/export/data1/www/lecturetranslator_new/
cd $ROOT
source /export/data1/www/lecturetranslator_new/venv/bin/activate

#sessionID=`python manage.py create_session_for_event --settings=config.settings.production ${eventID} en-EN-lecture_KIT -v 3`  
sessionID=`python manage.py create_session_for_event --settings=config.settings.production ${eventID} ${fingerprint} -v 3`  
echo $sessionID
sessionID=$(echo $sessionID | awk '{print $7}')

python manage.py read_stream_from_precomputed_data --settings=config.settings.production ${sessionID} ${src_lang} text /export/data1/www/lecturetranslator_new/temp/DIVAKIT/transcriptions/${showname}.timed.grouped.txt

python manage.py read_stream_from_precomputed_data --settings=config.settings.production ${sessionID} ${trg_lang} text /export/data1/www/lecturetranslator_new/temp/DIVAKIT/translations/${showname}.translated.timed.${trg_lang}

cp //project/wmt2012/LT/DIVAKIT/${showname}.mp4 $ROOT/media/streams/${sessionID}/video.mp4

python manage.py read_vtt_from_precomputed_data --settings=config.settings.production ${sessionID} ${src_lang} /export/data1/www/lecturetranslator_new/temp/DIVAKIT/transcriptions/${showname}.timed.grouped.txt

python manage.py read_vtt_from_precomputed_data --settings=config.settings.production ${sessionID} ${trg_lang} /export/data1/www/lecturetranslator_new/temp/DIVAKIT/translations/${showname}.translated.timed.${trg_lang}


