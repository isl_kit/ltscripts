#!/bin/bash
input="lst/lst_AGD_SS21.lst"
while IFS= read -r line
do
  IFS=', ' read -r -a line <<< "$line"
  showname=${line[0]}
  eventID=${line[1]}
  url=${line[2]}
  echo "$showname"
  echo "$eventID"
  echo "$url"
  wget -O "/var/www/lecturetranslator/media/eventintegration/$showname.mp4" "$url"
  echo "Downloaded $url"
  ./IntegrationClickURL.sh "$showname" $eventID "$url" 4 en de fr es it </dev/null
  echo "Integrated into event $eventID"
done < "$input"

