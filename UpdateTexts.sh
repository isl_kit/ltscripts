#!/bin/bash

showname=$1
eventID=$2
src_lang=$3
trg_langs=${@:4}
IFS=', ' read -r -a trg_langs <<< "$trg_langs"

ROOT="/var/www/lecturetranslator/"
DIVAKITSCRIPTS="/var/www/lecturetranslator/scripts/"
DIVAKITMEDIA="/var/www/lecturetranslator/media/eventintegration/"
VENVDIR="/home/lt/.virtualenv/lecturetranslator3"

## 1. Copy media from /project/mt2020/LTDep/DIVAKIT/
echo "Copy textual materials..."
bash ${DIVAKITSCRIPTS}/CopyFromMT2020.text.sh $showname 

## 2.1. Generate transcript segments with time
echo "Grouping transcript segments of file ${showname}.timed.txt..."
python ${DIVAKITSCRIPTS}/tools/insert_time.py ${DIVAKITMEDIA}/transcriptions/${showname}.timed.txt > ${DIVAKITMEDIA}/transcriptions/${showname}.timed.grouped.txt

## 2.2. Transfer time from transcript to translation
echo "Transfering time from the transcript to all the translations..."
bash ${DIVAKITSCRIPTS}/TransferTimeMany.sh $showname ${trg_langs[@]}


## 3. Get the session linked to the event

cd $ROOT
source $VENVDIR/bin/activate

sessionID=`python manage.py get_session_from_event --settings=config.settings.production ${eventID} -v 3`  
sessionID=$(echo $sessionID | awk '{print $5}')
echo "Updating text data on the session $sessionID of the event $eventID:"


echo "Updating the $src_lang transcript..."
python manage.py read_stream_from_precomputed_data --settings=config.settings.production ${sessionID} ${src_lang} text ${DIVAKITMEDIA}/transcriptions/${showname}.timed.grouped.txt

#cp //project/wmt2012/LT/DIVAKIT/${showname}.mp4 $ROOT/media/streams/${sessionID}/video.mp4

echo "Updating the $src_lang subtitle..."
python manage.py read_stream_from_precomputed_data --settings=config.settings.production ${sessionID} ${src_lang} text ${DIVAKITMEDIA}/transcriptions/${showname}.timed.grouped.txt

python manage.py read_vtt_from_precomputed_data --settings=config.settings.production ${sessionID} ${src_lang} ${DIVAKITMEDIA}/transcriptions/${showname}.timed.grouped.txt

for trg_lang in "${trg_langs[@]}"
do
#if [[ $trg_lang != "zh" ]]
#then
echo "Updating the $trg_lang subtitle"
python manage.py read_vtt_from_precomputed_data --settings=config.settings.production ${sessionID} ${trg_lang} ${DIVAKITMEDIA}/translations/${showname}.translated.timed.${trg_lang}
#fi
done

for trg_lang in "${trg_langs[@]}"
do
echo "Updating the $trg_lang translation"
python manage.py read_stream_from_precomputed_data --settings=config.settings.production ${sessionID} ${trg_lang} text ${DIVAKITMEDIA}/translations/${showname}.translated.timed.${trg_lang}
done


