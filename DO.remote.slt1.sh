#!/bin/bash
#####
# Remote execution of transcription/translations. Expects files and scripts as set up in in part "Preparing"
# Parameters:
# $1 unique string (showname)
# $2 input language tag
# $3- (from $3): output language tags. if there is none - only transcription)
#
# Returns:
#   0: No error
#  64: Language not supported
#  65: Wrong number of arguments
#  66: Wrong arguments provided
# 255: ssh error
# And all scp/rm error codes
#####

set -v # Verbose1
set -x # Verbose2
set -e # Exit on error

# ==== Preparing ====

if [[ "$#" -lt 2 ]]; then
	echo "$#"
	echo "Wrong number of arguments"
	exit 65
fi


TASKID="$1"
#IN_FILENAME="$2"
IN_LANG="$2"
OUT_LANGS=${@:3}
IFS=', ' read -r -a OUT_LANGS <<< "$OUT_LANGS"

#echo $OUT_LANGS
LT_HOST="matedub@lecture-translator.com"
CAL_HOST="fbk@localhost"
CAL_PORT="8085"

SSH1="ssh -i /export/data1/www/lecturetranslator_new/temp/DIVAKIT/id_ecdsa ${LT_HOST}"
SSH2="ssh -p 8085 ${CAL_HOST}"
SCP1="scp -i /export/data1/www/lecturetranslator_new/temp/DIVAKIT/id_ecdsa"
SCP2="scp"

SSHRED="$SSH1 $SSH2"

# Output directory of ctm depends on language
TMP_CAL_OUT_DIR="/export/data3/lectures/${TASKID}"
if [ "${IN_LANG}" == "de" ]; then
	TMP_CAL_OUT_DIR_CTM="${TMP_CAL_OUT_DIR}/04-seq2seq"
	REMOTE_CAL_SCRIPT="/export/data2/scripts/DO.slt_deen-gpu.sh"
elif [[ "${IN_LANG}" == "en" ]]; then
	TMP_CAL_OUT_DIR_CTM="${TMP_CAL_OUT_DIR}/12-seq2seq"
	REMOTE_CAL_SCRIPT="/export/data2/scripts/DO.slt_ende.sh"
else
	echo "Language '${IN_LANG}' is not supported."
	exit 64 
fi


REMOTE_CAL_IN_DIR="/export/data3/adc_in"
REMOTE_CAL_OUT_DIR="${TMP_CAL_OUT_DIR}"
REMOTE_LT_TMP_DIR="/home/matedub/tmp"


LOCAL_OUT_DIR="/tmp"
LOCAL_IN_FILE_DIR="/export/data1/www/lecturetranslator_new/temp/DIVAKIT"

# ==== Copy mp4 file to remote  ====
${SCP1} "${LOCAL_IN_FILE_DIR}/${TASKID}.mp4" "${LT_HOST}:${REMOTE_LT_TMP_DIR}/" # Copy infile local->LT
${SSH1} ${SCP2} -P ${CAL_PORT} "${REMOTE_LT_TMP_DIR}/${TASKID}.mp4" "${CAL_HOST}:${REMOTE_CAL_IN_DIR}/" # Copy infile LT->CAL

# ==== Start transcription ====
$SSHRED "$REMOTE_CAL_SCRIPT" "${TASKID}" "${TASKID}.mp4" 0 # Call CAL ctm script

# ==== Copy back ====

# == CTM file ==
REMOTE_CAL_OUT_DIR_CTM="${TMP_CAL_OUT_DIR_CTM}"
OUT_FILENAME_CTM="${TASKID}.ctm"
${SSH1} ${SCP2} -P ${CAL_PORT} "${CAL_HOST}:${REMOTE_CAL_OUT_DIR_CTM}/${OUT_FILENAME_CTM}" "${REMOTE_LT_TMP_DIR}/" # Copy result Cal->LT
${SCP1} "${LT_HOST}:${REMOTE_LT_TMP_DIR}/${OUT_FILENAME_CTM}" "${LOCAL_OUT_DIR}/" # Copy result LT->local TODO cleanup on error?
#$SSHRED rm "${REMOTE_CAL_IN_DIR}/${IN_FILENAME}" "${REMOTE_CAL_OUT_DIR}/${OUT_FILENAME}" # CAL
$SSH1 rm "${REMOTE_LT_TMP_DIR}/${TASKID}.*" # LT 
