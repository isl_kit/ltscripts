#!/bin/bash

#<<<<<<< Updated upstream
#=======

#>>>>>>> Stashed changes
set=$1
DIVAKIT="$2"
LTINTEGR_SCRIPT_DIR="$3"
#lang="$4"
#<<<<<<< Updated upstream
#target languages from the 4th parameters
#OUT_LANGS=${@:4}
#IFS=', ' read -r -a OUT_LANGS <<< "$OUT_LANGS"

#cd "${DIVAKIT}" # Does not work with the python script

#echo "Grouping transcript segments of file ${set}.timed.txt"
#python tools/insert_time.py transcriptions/${set}.timed.txt > transcriptions/${set}.timed.grouped.txt

#pwd # Debug
#=======
OUT_LANGS=${@:4}
IFS=', ' read -r -a OUT_LANGS <<< "$OUT_LANGS"

#>>>>>>> Stashed changes

for lang in "${OUT_LANGS[@]}"
do
echo "Inserting timestamps into translated file ${set}.translated.$lang"
#<<<<<<< Updated upstream
#=======
#python tools/transfer_time.py transcriptions/${set}.timed.grouped.txt translations/${set}.translated.$lang > translations/${set}.translated.timed.$lang
#>>>>>>> Stashed changes
python "${LTINTEGR_SCRIPT_DIR}/tools/transfer_time.py" "${DIVAKIT}/transcriptions/${set}.timed.grouped.txt" "${DIVAKIT}/translations/${set}.translated.$lang" > "${DIVAKIT}/translations/${set}.translated.timed.$lang"
done

