#!/bin/bash
#####
# Remote execution of transcription/translations. Expects files and scripts as set up in in part "Preparing"
# Parameters:
# $1 unique string (showname)
# $2 input language tag
# $3- (from $3): output language tags. if there is none - only transcription)
#
# Needs the LTINTEGR_MEDIA_DIR variable set to the local input media directory where the media file can be found.
# 
# Returns:
#   0: No error
#  64: Language not supported
#  65: Wrong number of arguments
#  66: Wrong arguments provided
#  67: Env variables not set correctly
# 255: ssh error
# And all scp/rm error codes
#####

#set -v # Verbose1
set -x # Verbose2
#set -e # Exit on error

# ==== Preparing ====

if [[ "$#" -lt 4 ]]; then
	echo "$#"
	echo "Wrong number of arguments"
	exit 65
fi

if [[ -z "${LTINTEGR_MEDIA_DIR}" ]]; then
	echo "This script \"$0\" needs the env-var LTINTEGR_MEDIA_DIR set."
	exit 67
fi

TASKID="$1"
GPUID="$2"
IN_LANG="$3"
OUT_LANGS=${@:4}
IFS=', ' read -r -a OUT_LANGS <<< "$OUT_LANGS"

#echo $OUT_LANGS
#LT_HOST="matedub@lecture-translator.com"
#CAL_HOST="fbk@localhost"
#CAL_PORT="8085"

LT_HOST="lt_remote_integration@i13hpc1.ira.uka.de"
#LT_HOST="mtasr@i13hpc1.ira.uka.de"
SSH1_IDENTITY="/var/www/lecturetranslator/.id_ecdsa_$(id -un)"
CAL_HOST="lt-worker@i13hpc54"
CAL_PORT="22"

SSH_COMMON_OPTIONS="-o StrictHostKeyChecking=no -o CheckHostIP=no -o IdentitiesOnly=yes"
SSH1_IDENTITY="-i /var/www/lecturetranslator/.id_ecdsa_$(id -un)"
#SSH1_IDENTITY="-i /var/www/lt2-kites/.id_ecdsa"

SSH1="ssh ${SSH_COMMON_OPTIONS} ${SSH1_IDENTITY} ${LT_HOST}"
SSH2="ssh ${SSH_COMMON_OPTIONS} -p ${CAL_PORT} ${CAL_HOST}"
SCP1="scp ${SSH_COMMON_OPTIONS} ${SSH1_IDENTITY}"
SCP2="scp ${SSH_COMMON_OPTIONS}"

SSHRED="$SSH1 $SSH2"

#SSH_COMMON_OPTIONS="-o StrictHostKeyChecking=no -o CheckHostIP=no -o IdentitiesOnly=yes" 
#SSH1_IDENTITY="/var/www/lecturetranslator/.id_ecdsa_$(id -un)"

#SSH1="ssh ${SSH_COMMON_OPTIONS} -i ${SSH1_IDENTITY} ${LT_HOST}"
#SSH2="ssh ${SSH_COMMON_OPTIONS} -p 8085 ${CAL_HOST}"
#SCP1="scp ${SSH_COMMON_OPTIONS} -i ${SSH1_IDENTITY}"
#SCP2="scp ${SSH_COMMON_OPTIONS}"

#SSHRED="$SSH1 $SSH2"

# Output directory of ctm depends on language
TMP_CAL_OUT_DIR="/export/data3/lectures/${TASKID}"
REMOTE_CAL_CHECK_GPU="/export/data2/scripts/check.free.GPU.sh"
TMP_CAL_OUT_DIR_CTM="${TMP_CAL_OUT_DIR}/asr"
if [ "${IN_LANG}" == "de" ]; then
	REMOTE_CAL_SCRIPT="/export/data2/scripts/DO.slt_de2all.sh"
elif [[ "${IN_LANG}" == "en" ]]; then
	REMOTE_CAL_SCRIPT="/export/data2/scripts/DO.slt_en2all.sh"
else
	echo "Language '${IN_LANG}' is not supported."
	exit 64 
fi


REMOTE_CAL_IN_DIR="/export/data3/adc_in"
REMOTE_CAL_OUT_DIR="${TMP_CAL_OUT_DIR}"
#REMOTE_LT_TMP_DIR="/home/matedub/tmp"
REMOTE_LT_TMP_DIR="/home_local/lt_remote_integration/tmp"


LOCAL_OUT_DIR="/tmp"
LOCAL_IN_FILE_DIR="${LTINTEGR_MEDIA_DIR}"

# ==== Copy mp4 file to remote  ====
${SCP1} "${LOCAL_IN_FILE_DIR}/${TASKID}.mp4" "${LT_HOST}:${REMOTE_LT_TMP_DIR}/" # Copy infile local->LT
${SSH1} ${SCP2} -P ${CAL_PORT} "${REMOTE_LT_TMP_DIR}/${TASKID}.mp4" "${CAL_HOST}:${REMOTE_CAL_IN_DIR}/" # Copy infile LT->CAL

# ==== Start transcription ====
#id=$($SSHRED "$REMOTE_CAL_CHECK_GPU") 
$SSHRED "$REMOTE_CAL_SCRIPT" "${TASKID}" "${TASKID}.mp4" "${GPUID}" "${IN_LANG}" "${@:4}" # Call CAL ctm script

# ==== Copy back ====

# == CTM file ==
REMOTE_CAL_OUT_DIR_CTM="${TMP_CAL_OUT_DIR_CTM}"
OUT_FILENAME_CTM="data.ctm"
${SSH1} ${SCP2} -P ${CAL_PORT} "${CAL_HOST}:${REMOTE_CAL_OUT_DIR_CTM}/${OUT_FILENAME_CTM}" "${REMOTE_LT_TMP_DIR}/" # Copy result Cal->LT
${SCP1} "${LT_HOST}:${REMOTE_LT_TMP_DIR}/${OUT_FILENAME_CTM}" "${LOCAL_OUT_DIR}/" # Copy result LT->local TODO cleanup on error?

# == Transcription ==
#/export/data3/lectures/$1/$1.timed.txt
OUT_FILENAME_TRANSC="${TASKID}.timed.txt"
${SSH1} ${SCP2} -P ${CAL_PORT} "${CAL_HOST}:${REMOTE_CAL_OUT_DIR}/${OUT_FILENAME_TRANSC}" "${REMOTE_LT_TMP_DIR}/" # Copy result Cal->LT
${SCP1} "${LT_HOST}:${REMOTE_LT_TMP_DIR}/${OUT_FILENAME_TRANSC}" "${LOCAL_OUT_DIR}/" # Copy result LT->local TODO cleanup on error?

# == VTT ==
#/export/data3/lectures/$1/$1.vtt
OUT_FILENAME_VTT="${TASKID}.vtt"
${SSH1} ${SCP2} -P ${CAL_PORT} "${CAL_HOST}:${REMOTE_CAL_OUT_DIR}/${OUT_FILENAME_VTT}" "${REMOTE_LT_TMP_DIR}/" # Copy result Cal->LT
${SCP1} "${LT_HOST}:${REMOTE_LT_TMP_DIR}/${OUT_FILENAME_VTT}" "${LOCAL_OUT_DIR}/" # Copy result LT->local TODO cleanup on error?

# == Translations ==
# /export/data3/lectures/$1//translation/output.gpu/$1.translated.$lang
for i in "${OUT_LANGS[@]}"
do
REMOTE_CAL_OUT_DIR_TRANSL="${TMP_CAL_OUT_DIR}/translation/output.gpu/"
OUT_FILENAME_TRANSL="${TASKID}.translated.$i"
${SSH1} ${SCP2} -P ${CAL_PORT} "${CAL_HOST}:${REMOTE_CAL_OUT_DIR_TRANSL}/${OUT_FILENAME_TRANSL}" "${REMOTE_LT_TMP_DIR}/" # Copy result Cal->LT
${SCP1} "${LT_HOST}:${REMOTE_LT_TMP_DIR}/${OUT_FILENAME_TRANSL}" "${LOCAL_OUT_DIR}/" # Copy result LT->local TODO cleanup on error?
done


# ==== Cleanup ====
#$SSHRED rm "${REMOTE_CAL_IN_DIR}/${IN_FILENAME}" "${REMOTE_CAL_OUT_DIR}/${OUT_FILENAME}" # CAL
$SSH1 rm "${REMOTE_LT_TMP_DIR}/${TASKID}.*" # LT 
