#!/bin/bash
input="lst_NN2020.sh"
while IFS= read -r line
do
  #echo "$line"
  IFS=', ' read -r -a line <<< "$line"
  showname=${line[0]}
  eventID=${line[1]}
  echo "$showname"
  echo "$eventID"
  bash UpdateTexts.sh $showname $eventID en fr
done < "$input"
