#!/bin/bash
set -e
set -v

showname="$1"
eventID="$2"
src_lang="$3"
trg_lang="$4"
video_file="$5"

#trg_langs=${@:4}
#IFS=', ' read -r -a trg_langs <<< "$trg_langs"


case "$src_lang" in
    en)
        fingerprint="en-EN-lecture_KIT"
        ;;
    de) 
        fingerprint="de-DE-lecture_KIT"
        ;;
    fr)
        fingerprint="fr-FR-lecture_KIT"
        ;;
    zh)
        fingerprint="zh-ZH-lecture_KIT"
        ;;
    es)
        fingerprint="es-ES-lecture_KIT"
        ;;
    it)
        fingerprint="it-IT-lecture_KIT"
        ;;
    vi)
        fingerprint="vi-VN-lecture_KIT"
        ;;
    tr)
        fingerprint="tr-TR-lecture_KIT"
        ;;
    pt)
        fingerprint="pt-PT-lecture_KIT"
        ;;
    ar)
        fingerprint="ar-AR-lecture_KIT"
        ;;
    nl)
        fingerprint="nl-NL-lecture_KIT"
        ;;
    ja)
        fingerprint="ja-JP-lecture_KIT"
        ;;
esac

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

ROOT="${SCRIPT_DIR}/.."

export LTINTEGR_SCRIPT_DIR="${ROOT}/scripts"

# TODO set these
#### !!! Configure these !!!
LTINTEGR_VIRTUALENV_DIR="/var/www/virtualenvs/lt2-kites"
export LTINTEGR_DJANGO_SETTINGS="--settings=config.settings.production"
export LTINTEGR_MEDIA_DIR="${ROOT}/media/eventintegration"
#export LTINTEGR_DJANGO_SETTINGS="--settings=config.settings.local"
#export LTINTEGR_MEDIA_DIR="${ROOT}/lecturetranslator/media/eventintegration"
####

######
# Check Directories
######

for i in "" "transcriptions" "translations" "vtt"; do
	dir="${LTINTEGR_MEDIA_DIR}/${i}"
	[[ -d "$dir" ]] || (echo -n "Directory \"$dir\" does not exist. Trying to create... ";  sleep 2; mkdir "$dir" && echo "Done." || (echo "Failed to create directory. Exiting."; exit -1))
done

#####
# Start
####

#export DIVAKIT=/export/data1/www/lecturetranslator_new/temp/DIVAKIT/

ln -s "${video_file}" "${LTINTEGR_MEDIA_DIR}/${showname}.mp4"

## 1. Ask GPU server to run the ASR/Seg/MT and copy the outputs back to /temp
#bash -ev $DIVAKIT/DO.remote.slt.sh "$showname" "${src_lang}" "${trg_langs[@]}"
if [ "${src_lang}" == "de" ]; then
    bash -ev "${LTINTEGR_SCRIPT_DIR}"/DO.remote.slt.bak.sh "$showname" de en fr es it
else [[ "${src_lang}" == "en" ]];
    bash -ev "${LTINTEGR_SCRIPT_DIR}"/DO.remote.slt.bak.sh "$showname" en de fr es it tr ja zh
fi

#bash -ev "${LTINTEGR_SCRIPT_DIR}"/DO.remote.slt.bak.sh "$showname" en de fr es it tr ja zh
#bash -ev "${LTINTEGR_SCRIPT_DIR}"/DO.remote.slt.sh "$showname" "${src_lang}" "${trg_lang}"

## 2. Copy media from /tmp
#bash -ev "${LTINTEGR_SCRIPT_DIR}"/CopyFromTmp.sh "$showname" "${LTINTEGR_MEDIA_DIR}" 
bash -ev "${LTINTEGR_SCRIPT_DIR}"/CopyFromTmpNew.sh "$showname" "${LTINTEGR_MEDIA_DIR}" 

## 3.1. Generate transcript segments with time
python "${LTINTEGR_SCRIPT_DIR}"/tools/insert_time.py "${LTINTEGR_MEDIA_DIR}/transcriptions/${showname}.timed.txt" > "${LTINTEGR_MEDIA_DIR}/transcriptions/${showname}.timed.grouped.txt"

## 3.2. Transfer time from transcript to all translations
#bash -ev $DIVAKIT/TransferTime.sh "$showname" "$DIVAKIT" "${trg_langs[@]}"
##### bash -ev "${LTINTEGR_SCRIPT_DIR}"/TransferTimeMany.sh "$showname" "${LTINTEGR_MEDIA_DIR}" "${LTINTEGR_SCRIPT_DIR}" "${trg_lang}"
if [ "${src_lang}" == "de" ]; then
    bash -ev "${LTINTEGR_SCRIPT_DIR}"/TransferTimeMany.sh "$showname" "${LTINTEGR_MEDIA_DIR}" "${LTINTEGR_SCRIPT_DIR}" en fr es it
else [[ "${src_lang}" == "en" ]];
    bash -ev "${LTINTEGR_SCRIPT_DIR}"/TransferTimeMany.sh "$showname" "${LTINTEGR_MEDIA_DIR}" "${LTINTEGR_SCRIPT_DIR}" de fr es it tr ja zh
fi


#bash -ev "${LTINTEGR_SCRIPT_DIR}"/TransferTimeMany.sh "$showname" "${LTINTEGR_MEDIA_DIR}" "${LTINTEGR_SCRIPT_DIR}" de fr es it tr ja zh

## 4. Create an empty session and link it to the event
cd "$ROOT"
source "${LTINTEGR_VIRTUALENV_DIR}/bin/activate"


#sessionID=`python manage.py create_session_for_event "${LTINTEGR_DJANGO_SETTINGS} "${eventID}" en-EN-lecture_KIT -v 3`  
sessionID=`python manage.py create_session_for_event --pevent "${LTINTEGR_DJANGO_SETTINGS}" "${eventID}" "${fingerprint}" -v 3`  
echo $sessionID
sessionID=$(echo $sessionID | awk '{print $7}')

## 5.1. Add transcript 
python manage.py read_stream_from_precomputed_data "${LTINTEGR_DJANGO_SETTINGS}" "${sessionID}" "${src_lang}" text "${LTINTEGR_MEDIA_DIR}/transcriptions/${showname}.timed.grouped.txt"

## 5.2. Add subtitle of transcript
python manage.py read_vtt_from_precomputed_data "${LTINTEGR_DJANGO_SETTINGS}" "${sessionID}" "${src_lang}" "${LTINTEGR_MEDIA_DIR}/transcriptions/${showname}.timed.grouped.txt"

if [ "${src_lang}" == "de" ]; then
    for trg_lang in en fr es it
    do
        echo "Updating the $trg_lang subtitle"
        python manage.py read_vtt_from_precomputed_data "${LTINTEGR_DJANGO_SETTINGS}" "${sessionID}" "${trg_lang}" "${LTINTEGR_MEDIA_DIR}/translations/${showname}.translated.timed.${trg_lang}"
    done

    for trg_lang in en fr es it
    do
        echo "Updating the $trg_lang translation"
        python manage.py read_stream_from_precomputed_data "${LTINTEGR_DJANGO_SETTINGS}" "${sessionID}" "${trg_lang}" text "${LTINTEGR_MEDIA_DIR}/translations/${showname}.translated.timed.${trg_lang}"
    done
else [[ "${src_lang}" == "en" ]];
    for trg_lang in de fr es it tr ja zh
    do
       echo "Updating the $trg_lang subtitle"
       python manage.py read_vtt_from_precomputed_data "${LTINTEGR_DJANGO_SETTINGS}" "${sessionID}" "${trg_lang}" "${LTINTEGR_MEDIA_DIR}/translations/${showname}.translated.timed.${trg_lang}"
    done

    for trg_lang in de fr es it tr ja zh
    do
       echo "Updating the $trg_lang translation"
       python manage.py read_stream_from_precomputed_data "${LTINTEGR_DJANGO_SETTINGS}" "${sessionID}" "${trg_lang}" text "${LTINTEGR_MEDIA_DIR}/translations/${showname}.translated.timed.${trg_lang}"
    done
fi

## 5.3. Add subtitles of translations 
#python manage.py read_vtt_from_precomputed_data "${LTINTEGR_DJANGO_SETTINGS}" "${sessionID}" "${trg_lang}" "${LTINTEGR_MEDIA_DIR}/translations/${showname}.translated.timed.${trg_lang}"

## 5.4. Add translations
#python manage.py read_stream_from_precomputed_data "${LTINTEGR_DJANGO_SETTINGS}" "${sessionID}" "${trg_lang}" text "${LTINTEGR_MEDIA_DIR}/translations/${showname}.translated.timed.${trg_lang}"



## 5.5. Move video/audio  
#mv "${LTINTEGR_MEDIA_DIR}/${showname}.mp4" "$ROOT/media/streams/${sessionID}/video.mp4"
