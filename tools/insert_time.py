#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys,os
import codecs,re

def merge_transcript(timed_transcript_file):

    with open(timed_transcript_file, "r") as fin:
        start_of_sentence = True
        sentence = ""
        for line in fin:
            ws = line.strip().split()
            if start_of_sentence:
                start = float(ws[0])
            if (ws[2][-1] in '.!?') or (ws[2][-2:] =='.\"') \
                or (ws[2][-2:] =='!\"') or (ws[2][-2:] =='?\"'):
                end =  float(ws[0]) + float(ws[1])
                sentence += " " + ws[2]
                #print("{0:.2f} {1:.2f} {2}".format(start, end-start-0.01, sentence))
                print("{0:.2f} {1:.2f} {2}".format(start, end, sentence))
                start_of_sentence = True
                sentence = ""
            else:
                start_of_sentence = False
                sentence += " " + ws[2]
        
        # If the talk doesn't end up with puncts, flush it out anyway
        if not start_of_sentence:
            end =  float(ws[0]) + float(ws[1])
            #print("{0:.2f} {1:.2f} {2}.".format(start, end-start-0.01, sentence))
            print("{0:.2f} {1:.2f} {2}.".format(start, end, sentence))

        
def main():
    args = sys.argv[1:]
    if len(args)==1:
        merge_transcript(args[0])
    else:
        raise SystemExit("Usage: python " + sys.argv[0] + " inputfile")

main()
