#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys,os
import codecs,re

def transfer_time(timed_grouped_file, translated_file):

    # Get the timestamps from the grouped file
    timestamps = []
    with open(timed_grouped_file, "r") as ftime:
        for line in ftime:
            ws = line.strip().split()
            timestamps.append(ws[0] + " " + ws[1])
 
        
    # Insert the timestamps to the translation file
    with open(translated_file, "r") as ftran:
        start = None
        for i, line in enumerate(ftran):
            try:
                print("{0} {1}".format(timestamps[i], line.strip()))
            except IndexError:
                # translation has more lines than transcript
                # add a pseudo timestamp
                if not start:
                    start = float(timestamps[i-1].split()[0])
                    #start_next = start + float(timestamps[i-1].split()[1]) + 0.01     
                    start_next = float(timestamps[i-1].split()[1]) + 0.01
                    end_next = start_next + 2.9  #just a pseudo duration       
                    #print("{0:.2f} 2.9 {1}".format(start_next, line.strip()))
                    print("{0:.2f} {1:.2f} {2}".format(start_next, end_next, line.strip()))
                else:
                    #print("{0.2f} 2.9 {1}".format(start + 2.91, line.strip()))
                    print("{0:.2f} {1:.2f} {2}".format(start + 2.91, start + 5.81, line.strip()))
                    start = start + 2.91
def main():
    args = sys.argv[1:]
    if len(args)==2:
        transfer_time(args[0], args[1])
    else:
        raise SystemExit("Usage: python " + sys.argv[0] + \
                        "timed_grouped_file translated_file")

main()
