#!/bin/bash

video_file="$1"
subtitle="$2"
showname="$3"
eventID="$4"
fingerprint="$5"

echo $video_file
echo $subtitle
echo $showname
echo $eventID
echo $fingerprint

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

ROOT="${SCRIPT_DIR}/.."
export MEDIA_DIR="${ROOT}/media/personal/${eventID}"


ffmpeg -y -i $video_file -vf subtitles=$subtitle "${MEDIA_DIR}/${showname}.${fingerprint}.mp4"
