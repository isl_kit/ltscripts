#!/bin/bash

showname=$1
eventID=$2
src_lang=$3
trg_langs=${@:4}
IFS=', ' read -r -a trg_langs <<< "$trg_langs"

case "$src_lang" in
    en)
        fingerprint="en-EN-lecture_KIT"
        ;;
    de)
        fingerprint="de-DE-lecture_KIT"
        ;;
    fr)
        fingerprint="fr-FR-lecture_KIT"
        ;;
    zh)
        fingerprint="zh-ZH-lecture_KIT"
        ;;
    es)
        fingerprint="es-ES-lecture_KIT"
        ;;
    it)
        fingerprint="it-IT-lecture_KIT"
        ;;
    vi)
        fingerprint="vi-VN-lecture_KIT"
        ;;
    tr)
        fingerprint="tr-TR-lecture_KIT"
        ;;
    pt)
        fingerprint="pt-PT-lecture_KIT"
        ;;
    ar)
        fingerprint="ar-AR-lecture_KIT"
        ;;
    nl)
        fingerprint="nl-NL-lecture_KIT"
        ;;
    ja)
        fingerprint="ja-JP-lecture_KIT"
        ;;
esac

ROOT="/var/www/lecturetranslator"
LTINTEGR_VIRTUALENV_DIR="/home/lt/.virtualenv/lecturetranslator3"
export LTINTEGR_SCRIPT_DIR="${ROOT}/scripts"
export LTINTEGR_MEDIA_DIR="${ROOT}/media/eventintegration"


######
# Check Directories
######
for i in "" "transcriptions" "translations" "vtt"; do
        dir="${LTINTEGR_MEDIA_DIR}/${i}"
        [[ -d "$dir" ]] || (echo -n "Directory \"$dir\" does not exist. Trying to create... ";  sleep 2; mkdir "$dir" && echo "Done." || (echo "Failed to create directory. Exiting."; exit -1))
done

#####
# Start
####


## 1. Copy media from /project/mt2020/LTDep/DIVAKIT/
echo "Copy textual materials..."
#bash ${DIVAKITSCRIPTS}/CopyFromMT2020.text.sh $showname 
bash -ev "${LTINTEGR_SCRIPT_DIR}"/CopyFromTmp.sh "$showname" "${LTINTEGR_MEDIA_DIR}"
#bash -ev "${LTINTEGR_SCRIPT_DIR}"/CopyFromTmp.sh "$showname" "${LTINTEGR_MEDIA_DIR}"

## 2.1. Generate transcript segments with time
echo "Grouping transcript segments of file ${showname}.timed.txt..."
python "${LTINTEGR_SCRIPT_DIR}"/tools/insert_time.py "${LTINTEGR_MEDIA_DIR}/transcriptions/${showname}.timed.txt" > "${LTINTEGR_MEDIA_DIR}/transcriptions/${showname}.timed.grouped.txt"

## 2.2. Transfer time from transcript to translation

echo "Transfering time from the transcript to all the translations..."
#bash ${DIVAKITSCRIPTS}/TransferTimeMany.sh $showname ${trg_langs[@]}
bash -ev "${LTINTEGR_SCRIPT_DIR}"/TransferTimeMany.sh "$showname" "${LTINTEGR_MEDIA_DIR}" "${LTINTEGR_SCRIPT_DIR}" "${trg_langs[@]}"

## 3. Get the session linked to the event
cd "$ROOT"
source "${LTINTEGR_VIRTUALENV_DIR}/bin/activate"

sessionID=`python manage.py get_session_from_event --settings=config.settings.production ${eventID} -v 3`  
sessionID=$(echo $sessionID | awk '{print $5}')
echo "Updating text data on the session $sessionID of the event $eventID:"


echo "Updating the $src_lang transcript..."
python manage.py read_stream_from_precomputed_data --settings=config.settings.production "${sessionID}" "${src_lang}" text "${LTINTEGR_MEDIA_DIR}/transcriptions/${showname}.timed.grouped.txt"

echo "Updating the $src_lang subtitle..."
python manage.py read_vtt_from_precomputed_data --settings=config.settings.production "${sessionID}" "${src_lang}" "${LTINTEGR_MEDIA_DIR}/transcriptions/${showname}.timed.grouped.txt"


for trg_lang in "${trg_langs[@]}"
do
echo "Updating the $trg_lang subtitle"
python manage.py read_vtt_from_precomputed_data --settings=config.settings.production "${sessionID}" "${trg_lang}" "${LTINTEGR_MEDIA_DIR}/translations/${showname}.translated.timed.${trg_lang}"
done

for trg_lang in "${trg_langs[@]}"
do
echo "Updating the $trg_lang translation"
python manage.py read_stream_from_precomputed_data --settings=config.settings.production "${sessionID}" "${trg_lang}" text "${LTINTEGR_MEDIA_DIR}/translations/${showname}.translated.timed.${trg_lang}"
done


